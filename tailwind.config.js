module.exports = {
  theme: {
    extend: {
      fontFamily: {
        proza: ['Proza Libre', 'sans-serif'],
      },
      fontSize: {
        '7xl' : '5rem',
        '8xl' : '8rem',
        '9xl' : '10rem',
        '10xl': '12rem'
      },
      inset: {
        'unit': '1%',
        'quarter': '25%'
      }
    }
  },
  variants: {},
  plugins: []
}
